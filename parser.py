import sys
from html_data_parser import HtmlDataFinder
from html_data_writer import HtmlDataWriter, FileNameFromUrl

for url in sys.argv[1:]:
    finder = HtmlDataFinder()
    data = finder.find_data(url)

    writer = HtmlDataWriter(FileNameFromUrl.url_to_name(url), data)
    writer.perform()