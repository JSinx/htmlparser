import re
import os
from urllib.parse import urlparse


class FileNameFromUrl(object):

    @staticmethod
    def url_to_name(url):
        url_parts = urlparse(url)
        return url_parts.netloc + url_parts.path


class HtmlDataWriter(object):

    def __init__(self, file_name, data):
        self.first_line = True
        self.data = data
        self.file_name = file_name
        self.rules = {'h1': self._add_main_header,
                      'h2': self._add_second_header,
                      'else': self._add_text}

        super().__init__()

    def perform(self):
        self._create_dir(self.file_name)
        self.file = open(self.file_name + '/text.txt', 'w')

        for record in self.data:
            self._first_empty_line()
            row_line = self._prepare_line(record['text'])

            if record['type'] in self.rules:
                self.rules[record['type']](row_line, self.file)
            else:
                self.rules['else'](row_line, self.file)

        self.file.close()

    def _add_main_header(self, row, file):
        self.file.write('-'*80 + '\n')
        for strline in self._split_row_by_length(row):
            self.file.write('|  ' + strline + '\n')
        self.file.write('-'*80 + '\n')

    def _add_second_header(self, row, file):
        for strline in self._split_row_by_length(row):
            self.file.write(strline + '\n')
        self.file.write('\n')

    def _add_text(self, row, file):
        for strline in self._split_row_by_length(row):
            self.file.write(strline + '\n')

    def _first_empty_line(self):
        if not self.first_line:
            self.file.write('\n')
        else:
            self.first_line = False

    def _prepare_line(self, row):
        return self._href_replace(row)

    @staticmethod
    def _href_replace(row):
        href_reg = r'<a[^>]*?href=\"([\S]*?)\"[^>]*?>([\S\s]*?)<\/a>'

        row_new = row

        for link in re.finditer(href_reg, row):
            place_str = link.group(2) + ' [' + link.group(1) + '] '
            row_new = row_new.replace(link.group(), place_str)

        return row_new

    @staticmethod
    def _split_row_by_length(row):
        reg_length = r'[\S ]{1,80}$|[\S ]{1,80}[\s]|\[[\s\S]*?\]'
        return re.findall(reg_length, row)

    @staticmethod
    def _create_dir(path):
        os.makedirs(path, exist_ok=True)



