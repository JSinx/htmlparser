# -*- coding: utf-8 -*-
import urllib.request as urllib
import urllib.parse as urlencode
import re
import html

class HtmlDataFinder(object):
    def find_data(self, url):
        self._get_data_from_url(url)
        self._remove_waste_data()
        self._get_useful_data()
        return self.result_data

    # private

    def _remove_waste_data(self):
        waste_options = [
            {'reg_exp': r"<script\b[^>]*>([\s\S]*?)</script>", 'repl': ''},
            {'reg_exp': r"<noscript\b[^>]*>([\s\S]*?)</noscript>", 'repl': ''},
            {'reg_exp': r"<div\b[^>]*>|</div>", 'repl': ''},
            {'reg_exp': r"<meta\b[^>]*>", 'repl': ''},
            {'reg_exp': r"<!--*([\s\S]*?)-->", 'repl': ''},
            {'reg_exp': r"<aside\b[^>]*>([\s\S]*?)</aside>", 'repl': ''},  # lenta.ru
            {'reg_exp': r"<article class=\"article\">[\s\S]*?</article>", 'repl': ''},  # gazeta.ru
            {'reg_exp': r"<section\b[^>]*>([\s\S]*?)</section>", 'repl': ''},  # lenta.ru
            {'reg_exp': r"<nav\b[^>]*>([\s\S]*?)</nav>", 'repl': ''},  # lenta.ru
            {'reg_exp': r"<footer\b[^>]*>([\s\S]*?)</footer>", 'repl': ''},  # gazeta.ru
            {'reg_exp': r"<noindex\b[^>]*>([\s\S]*?)</noindex>", 'repl': ''},  # gazeta.ru
            {'reg_exp': r"<span\b[^>]*>|</span>", 'repl': ''},
            {'reg_exp': r"<b>|</b>", 'repl': ''},
            {'reg_exp': r"<blockquote\b[^>]*>([\s\S]*?)</blockquote>", 'repl': ''}, # moslenta.ru
        ]

        for waste_data in waste_options:
            self.data = re.sub(waste_data['reg_exp'], waste_data['repl'], self.data)

    def _get_useful_data(self):
        body_reg = r"<body\b[^>]*>([\s\S]*?)</body>"
        body_data = re.search(body_reg, self.data).group()

        reg_headers = r"<h1\b[^>]*>(?P<h1>[\s\S]*?)</h1>|<h\d\b[^>]*>(?P<h2>[\s\S]*?)</h\d>"
        reg_exp = r"<p\b[^>]*>(?P<text>[\s\S]*?)</p>"
        useful_regexp = '|'.join([reg_headers, reg_exp])

        self.result_data = []
        for match in re.finditer(useful_regexp, body_data):
            group = match.lastgroup
            self.result_data.append({'type': group, 'text': match.group(group)})

    def _get_data_from_url(self, url):
        data = urllib.urlopen(url)
        charset = self._get_charset(data)
        data = data.read().decode(charset)
        self.data = html.unescape(data)

    def _get_charset(self, data):
        content_type = data.getheader('content-type')
        match = re.search(r'charset=([^\s;]+)', content_type)
        if match:
            return match.group(1).strip('"\'').lower()
